package pl.sda.dto;

/**
 * Created by adrian on 28.03.17.
 */
public class MessageDto {

    private MessageDtoType type;
    String text;

    public MessageDto(MessageDtoType type, String text) {
        this.type = type;
        this.text = text;
    }

    public MessageDto() {

    }

    public MessageDtoType getType() {
        return type;
    }

    public String getText() {
        return text;
    }


    public enum MessageDtoType {
        INFO("alert alert-info"),
        ERROR("alert alert-warning"),
        WARN("alert alert-danger"),
        SUCCESS("alert alert-success");

        private String cssClass;

        MessageDtoType(String cssClass){
            this.cssClass = cssClass;
        }

        @Override
        public String toString() {
            return this.cssClass;
        }
    }
}
