package pl.sda.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adrian on 03.04.17.
 */
public class SearchDto {

    private String title;

    /**
     * książka || użytkownik
     */
    private Integer searchType = 1;

    /**
     * treść wyszukiwania
     */
    private String query;

    /**
     * Lista wyników
     */
    List<SearchObjectDto> resultList = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getSearchType() {
        return searchType;
    }

    public void setSearchType(Integer searchType) {
        this.searchType = searchType;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<SearchObjectDto> getResultList() {
        return resultList;
    }

    public void setResultList(List<SearchObjectDto> resultList) {
        this.resultList = resultList;
    }
}
