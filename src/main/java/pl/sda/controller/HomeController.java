/*
 * Copyright 2012-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pl.sda.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.MessageDto;
import pl.sda.dto.SearchDto;
import pl.sda.dto.SearchObjectDto;
import pl.sda.service.BookService;
import pl.sda.service.MessageService;
import pl.sda.service.UserService;

import java.util.List;

/**
 * @author Rob Winch
 * @author Doo-Hwan Kwak
 */
@Controller
@RequestMapping("/")
public class HomeController {

    final static Logger LOG = LogManager.getLogger();

    @Autowired
    private MessageService messageService;
    @Autowired
    private BookService bookService;
    @Autowired
    private UserService userService;

    @GetMapping("/form")
    public ModelAndView form(ModelMap modelMap) {

        modelMap.addAttribute("messageDto", new MessageDto());
        return new ModelAndView("messages/form", modelMap);
    }

    @RequestMapping("/{id}")
    public ModelAndView edit(@PathVariable(name = "id") Integer id, ModelMap modelMap) {

        System.out.println(id);


        return new ModelAndView("messages/form", modelMap);
    }

    @PostMapping("/form")
    public ModelAndView submit(@ModelAttribute(name = "messageDto") MessageDto messageDto) {

        LOG.debug("saved!");
        return new ModelAndView("redirect:/");
    }

    /**
     * MEoda odpowiedzialna za wyświetlenie wyszukiwarki (strony głównej)
     */
    @GetMapping
    public ModelAndView list(ModelMap modelMap) {
        modelMap.addAttribute("searchDto", new SearchDto());
        return new ModelAndView("messages/list", modelMap);
    }

    /**
     * MEoda odpowiedzialna za wyświetlenie wyników wyszukiwania
     */
    @PostMapping
    public ModelAndView listResults(@ModelAttribute SearchDto searchDto, ModelMap modelMap) {
        System.out.println(searchDto.getQuery());
        if (1 == searchDto.getSearchType()) {
            List<SearchObjectDto> resultList = bookService.searchByTitle(searchDto.getQuery());
            searchDto.setResultList(resultList);
        } else if (2 == searchDto.getSearchType()) {
            List<SearchObjectDto> resultList = userService.searchByUser(searchDto.getQuery());
            searchDto.setResultList(resultList);
        }
        modelMap.addAttribute("searchDto", searchDto);
        return new ModelAndView("messages/list", modelMap);
    }

}


























