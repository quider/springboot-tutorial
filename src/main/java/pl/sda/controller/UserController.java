package pl.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.BookDto;
import pl.sda.dto.SearchDto;
import pl.sda.dto.UserDto;
import pl.sda.service.MessageService;
import pl.sda.service.RentService;
import pl.sda.service.UserService;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    @Autowired
    RentService rentService;
    @Autowired
    private MessageService messageService;

    @GetMapping("/add")
    public ModelAndView addUser(ModelMap modelMap) {
        modelMap.addAttribute("userDto", new UserDto());
        return new ModelAndView("user/form", modelMap);
    }

    @PostMapping("/add")
    public ModelAndView saveUser(@ModelAttribute UserDto userDto, ModelMap modelMap) {
        userService.save(userDto);
        modelMap.addAttribute("userDto", new UserDto());
        return new ModelAndView("user/form", modelMap);
    }

    @GetMapping("/search")
    public ModelAndView searchUsersToReturnBook(ModelMap modelMap){
        modelMap.addAttribute("searchDto", new SearchDto());
        return new ModelAndView("rent/usersearch", modelMap);
    }

    @PostMapping("/search")
    public ModelAndView resultUsersToReturnBook(@ModelAttribute SearchDto searchDto, ModelMap modelMap){
        searchDto.setResultList(userService.searchByUser(searchDto.getQuery()));
        modelMap.addAttribute("searchDto", searchDto);
        return new ModelAndView("rent/usersearch", modelMap);
    }

    @GetMapping("/id/{id}")
    public ModelAndView returnBook(@PathVariable(name = "id") Integer id , ModelMap modelMap){
        UserDto userDto = userService.getUserById(id);
        List<BookDto> rentDtoList = rentService.getUserBooks(id);
        modelMap.addAttribute("userDto", userDto);
        modelMap.addAttribute("masterId", id);
        modelMap.addAttribute("rentDtoList", rentDtoList);
        return new ModelAndView("rent/userprofile", modelMap);
    }
    
    @GetMapping("/return")
    public ModelAndView returnUsersBook(@RequestParam("userId") Integer userId, @RequestParam("bookId") Integer bookId, ModelMap modelMap) {
        try {
            rentService.returnIt(userId, bookId);
        } catch (Exception e) {
            messageService.addErrorMessage("Błąd. Skontaktuj się ze swoim deweloperem.");
            e.printStackTrace();
        }
        messageService.addSuccessMessage("Książka została oddana!");
        return new ModelAndView("redirect:/", modelMap);
    }
}
