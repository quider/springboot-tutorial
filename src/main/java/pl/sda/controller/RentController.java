package pl.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.SearchDto;
import pl.sda.dto.SearchObjectDto;
import pl.sda.model.Book;
import pl.sda.service.BookService;
import pl.sda.service.MessageService;
import pl.sda.service.RentService;
import pl.sda.service.UserService;

import java.util.List;

/**
 * Created by adrian on 05.04.17.
 */
@Controller
@RequestMapping("/rent")
public class RentController {

    @Autowired
    private RentService rentService;
    @Autowired
    private UserService userService;
    @Autowired
    private BookService bookService;
    @Autowired
    private MessageService messageService;

    @GetMapping("/book")
    public ModelAndView rentByBookId(@RequestParam("id") Integer id, ModelMap modelMap) {
        SearchDto searchDto = new SearchDto();
        String title = bookService.getBookTitleById(id);
        //ustawiamy typ wyszukiwania na 2 = uzytkownicy
        searchDto.setSearchType(2);
        searchDto.setQuery("");
        searchDto.setTitle(title);
        modelMap.addAttribute("masterId", id);
        modelMap.addAttribute("searchDto", searchDto);
        return new ModelAndView("rent/list", modelMap);
    }

    @PostMapping("/book")
    public ModelAndView viewPossibleUsersToRent(@RequestParam("id") Integer id, @ModelAttribute SearchDto searchDto, ModelMap modelMap) {
        List<SearchObjectDto> resultList = userService.searchByUser(searchDto.getQuery());
        searchDto.setResultList(resultList);
        modelMap.addAttribute("masterId", id);
        return new ModelAndView("rent/list", modelMap);
    }

    @GetMapping("/user")
    public ModelAndView rentByUserId(@RequestParam("id") Integer id, ModelMap modelMap) {
        SearchDto searchDto = new SearchDto();
        //ustawiamy typ wyszukiwania na 1 = książka
        searchDto.setSearchType(1);
        searchDto.setQuery("");
        searchDto.setTitle(userService.getUsername(id));
        modelMap.addAttribute("masterId", id);
        modelMap.addAttribute("searchDto", searchDto);
        return new ModelAndView("rent/list", modelMap);
    }


    @PostMapping("/user")
    public ModelAndView viewPossibleBooksToRent(@RequestParam("id") Integer id, @ModelAttribute SearchDto searchDto, ModelMap modelMap) {
        List<SearchObjectDto> resultList = bookService.searchByTitle(searchDto.getQuery());
        searchDto.setResultList(resultList);
        modelMap.addAttribute("masterId", id);
        return new ModelAndView("rent/list", modelMap);
    }



    @GetMapping("/touser")
    public ModelAndView rentToUser(@RequestParam("id") Integer id, @RequestParam("userId") Integer userId, ModelMap modelMap) {
        try {
            rentService.rentBookToUser(id, userId);
        } catch (Exception e) {
            messageService.addErrorMessage("Błąd. Skontaktuj się ze swoim deweloperem.");
            e.printStackTrace();
        }
        messageService.addSuccessMessage("Książka została wypożyczona!");
        return new ModelAndView("redirect:/", modelMap);
    }

    @GetMapping("/thisbook")
    public ModelAndView rentThisBook(@RequestParam("id") Integer id, @RequestParam("userId") Integer userId, ModelMap modelMap) {
        try {
            rentService.rentBookToUser(id, userId);
        } catch (Exception e) {
            messageService.addErrorMessage("Błąd. Skontaktuj się ze swoim deweloperem.");
            e.printStackTrace();
        }
        messageService.addSuccessMessage("Książka została wypożyczona!");
        return new ModelAndView("redirect:/", modelMap);
    }
}
