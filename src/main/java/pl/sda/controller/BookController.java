package pl.sda.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.dto.BookDto;
import pl.sda.service.BookService;
import pl.sda.service.MessageService;

/**
 * Created by adrian on 04.04.17.
 */
@Controller
public class BookController {

    @Autowired
    private BookService bookService;
    @Autowired
    private MessageService messageService;

    /**
     * Odpowiedzialna tylko za wyświetlenie formularza do dodania książki
     * @param modelMap
     * @return
     */
    @GetMapping("/book/add")
    public ModelAndView addBook(ModelMap modelMap){
        modelMap.addAttribute("bookDto", new BookDto());
        return new ModelAndView("book/form", modelMap);
    }

    @PostMapping("/book/add")
    public ModelAndView saveBook(@ModelAttribute BookDto bookDto, ModelMap modelMap){
        try {
            bookService.save(bookDto);
            modelMap.addAttribute("bookDto", new BookDto());
            messageService.addSuccessMessage("Dodana książka!");
        } catch (Exception e) {
            messageService.addErrorMessage(e.getMessage());
        }
        return new ModelAndView("book/form", modelMap);
    }
}























