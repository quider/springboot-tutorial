package pl.sda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.dto.SearchObjectDto;
import pl.sda.dto.UserDto;
import pl.sda.model.User;
import pl.sda.repository.UserRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public List<SearchObjectDto> searchByUser(String query) {
        List<User> users = userRepository.findAllByUsername(query);
        List<SearchObjectDto> resultList = users.stream().map(u -> new SearchObjectDto(u.getId(),u.getUsername(), u.getTelephone())).collect(Collectors.toList());
        return resultList;
    }

    public void save(UserDto userDto) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(userDto.getBirthDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        User user = new User(userDto.getUsername(), new Date(), date, userDto.getMail(), userDto.getTelephone());
        userRepository.save(user);
    }

    public String getUsername(Integer id) {
        return userRepository.findOne(id).getUsername();
    }

    public UserDto getUserById(Integer id) {
        UserDto userDto = new UserDto();
        User user = userRepository.findOne(id);
        userDto.setBirthDate(user.getBirthDate()==null?"nieznana":user.getBirthDate().toString());
        userDto.setId(user.getId());
        userDto.setMail(user.getMail());
        userDto.setTelephone(user.getTelephone());
        userDto.setUsername(userDto.getUsername());
        return userDto;
    }
}