package pl.sda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.dto.MessageDto;
import pl.sda.model.Message;
import pl.sda.repository.MessageReposiory;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by adrian on 01.04.17.
 */
@Service
public class MessageService {
    public static final String NOTIFY_MSG_SESSION_KEY = "notificationMessages";

    @Autowired
    private HttpSession httpSession;

    /**
     * @param msg
     */
    public void addInfoMessage(String msg) {
        addNotificationMessage(MessageDto.MessageDtoType.INFO, msg);
    }

    /**
     *
     * @param msg
     */
    public void addSuccessMessage(String msg){
        addNotificationMessage(MessageDto.MessageDtoType.SUCCESS, msg);
    }

    /**
     *
     * @param msg
     */
    public void addWarnMessage(String msg){
        addNotificationMessage(MessageDto.MessageDtoType.WARN, msg);
    }

    /**
     * @param msg
     */
    public void addErrorMessage(String msg) {
        addNotificationMessage(MessageDto.MessageDtoType.ERROR, msg);
    }

    /**
     * @param type
     * @param msg
     */
    private void addNotificationMessage(MessageDto.MessageDtoType type, String msg) {
        List<MessageDto> notifyMessages = (List<MessageDto>) httpSession.getAttribute(NOTIFY_MSG_SESSION_KEY);
        if (notifyMessages == null) {
            notifyMessages = new ArrayList<MessageDto>();
        }
        notifyMessages.add(new MessageDto(type, msg));
        httpSession.setAttribute(NOTIFY_MSG_SESSION_KEY, notifyMessages);
    }
}




















