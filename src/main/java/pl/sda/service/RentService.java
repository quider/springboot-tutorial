package pl.sda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.controller.RentDto;
import pl.sda.dto.BookDto;
import pl.sda.model.Book;
import pl.sda.model.Rent;
import pl.sda.model.User;
import pl.sda.repository.BookRepository;
import pl.sda.repository.RentRepository;
import pl.sda.repository.UserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adrian on 05.04.17.
 */
@Service
public class RentService {
    @Autowired
    private RentRepository rentRepository;
    @Autowired
    private BookRepository bookRepository;
    @Autowired
    private UserRepository userRepository;

    public void rentBookToUser(Integer bookId, Integer userId) {
        User user = userRepository.findOne(userId);
        Book book = bookRepository.findOne(bookId);
        Rent rent = new Rent();
        rent.setBook(book);
        rent.setUser(user);
        rent.setRentDate(new Date());
        if (user == null || book == null) {
            throw new NullPointerException("!!!");
        }
        rentRepository.save(rent);
    }

    public List<BookDto> getUserBooks(Integer id) {
        User one = userRepository.findOne(id);
        List<Rent> rentBooks= rentRepository.findUserBooks(one);
        ArrayList<BookDto> bookDtoArrayList = new ArrayList<BookDto>();
        for (Rent rent : rentBooks) {
            Book rentBook = rent.getBook();
            BookDto bookDto = new BookDto();
            bookDto.setId(rentBook.getId());
            bookDto.setAuthor(rentBook.getAuthor());
            bookDto.setDescription(rentBook.getDescription());
            bookDto.setISBN(rentBook.getISBN());
            bookDto.setTitle(rentBook.getTitle());
            bookDtoArrayList.add(bookDto);
        }
        return bookDtoArrayList;
    }


	public void returnIt(Integer userId, Integer bookId) {
		User user = userRepository.findOne(userId);
		Book book = bookRepository.findOne(bookId);
		Rent forReturn = rentRepository.findForReturn(user, book);
		forReturn.setReturnDate(new Date());
		rentRepository.save(forReturn);
	}
}





























