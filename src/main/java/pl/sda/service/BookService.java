package pl.sda.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.sda.dto.BookDto;
import pl.sda.dto.SearchObjectDto;
import pl.sda.model.Book;
import pl.sda.repository.BookRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepository;

    /**
     * Wyszukuje ksiązek po tytule. Nie trzeba podawac calego tytulu.
     * Wystarczy fragment.
     * @param query na przyklad szukajac książki "Pan Tadeusz" wystarczy wpisać "Pa"
     * @return Lista obiektów transportowych z danymi książek
     */
    public List<SearchObjectDto> searchByTitle(String query) {
        List<Book> booksCollection = bookRepository.findByTitle(query);
        ArrayList<SearchObjectDto> searchObjectDtos = new ArrayList<>();
        for (Book book : booksCollection) {
            SearchObjectDto searchObjectDto = new SearchObjectDto(book.getId(),book.getTitle(), book.getISBN());
            searchObjectDtos.add(searchObjectDto);
        }
        return  searchObjectDtos;
    }

    /**
     * zapisuje Book utworzony za pomocą bookDto do bazy danych
     * @param bookDto dane pobrane z formy
     */
    public void save(BookDto bookDto) {
        if(bookDto.getTitle() == null || bookDto.getTitle().isEmpty())
            throw new NullPointerException("Książka musi mieć tytuł!");
        Book book = new Book(bookDto.getTitle(),
                bookDto.getSubtitle(), bookDto.getAuthor(), bookDto.getISBN(), bookDto.getReleaseYear(),
                bookDto.getReleaser(), bookDto.getRelease(), bookDto.getDescription(), new Date());
        bookRepository.save(book);
    }

    /**
     * na podstawie ID pobierany jest tytuł książki
     * @param id
     * @return
     */
    public String getBookTitleById(Integer id) {
        Book one = bookRepository.findOne(id);
        return one.getTitle();
    }
}
