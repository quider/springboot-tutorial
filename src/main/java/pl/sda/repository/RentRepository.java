package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.model.Book;
import pl.sda.model.Rent;
import pl.sda.model.User;

import java.util.List;

/**
 * Created by adrian on 05.04.17.
 */
@Repository
public interface RentRepository extends JpaRepository<Rent, Integer> {

    @Query("from Rent r where r.user = :user")
    List<Rent> findUserBooks(@Param("user")User one);
    
    @Query("from Rent r where r.user = :user and r.book = :book")
    Rent findForReturn(@Param("user")User user, @Param("book")Book book);
}
