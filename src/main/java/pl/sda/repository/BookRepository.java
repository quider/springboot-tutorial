package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.sda.model.Book;

import java.util.List;

/**
 * Created by adrian on 03.04.17.
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

    @Query("from Book b where b.title like %:title%")
    List<Book> findByTitle(@Param("title") String title);
}
