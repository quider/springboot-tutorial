package pl.sda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.model.Message;

/**
 * Created by adrian on 01.04.17.
 */
public interface MessageReposiory extends JpaRepository<Message, Integer> {
}
